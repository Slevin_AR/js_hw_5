
const array = [1, 3, 5, 7, 9];


function double(num) {
    return num * 2;
} 

function map(fn, arr) {
    const result = [];
    for (let i=0; i < arr.length; i++) {
        result[i] = fn(arr[i]);
    }
    return result
}

const arrayRes = map(double, array);

document.write(`Вивід результату ${arrayRes.toString()} <br>`);

//////////////////////////////////////////////////////////////////

function checkAge(age) {
    return  age > 18 ? true : confirm(`Родители розрешили?`) ;
}
document.write(`Результат ${checkAge(19)}<br>`);
document.write(`Результат ${checkAge(11)}<br>`);

function checkAge2(age) {
    return  age > 18 || confirm(`Родители розрешили?`) ;
}

document.write(`Результат ${checkAge2(19)}<br>`);
document.write(`Результат ${checkAge2(11)}<br>`);